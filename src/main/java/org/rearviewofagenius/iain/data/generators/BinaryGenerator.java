package org.rearviewofagenius.iain.data.generators;

import java.util.concurrent.LinkedBlockingDeque;

public class BinaryGenerator extends Generator {
	static String format = "octets";

	int bytes = 8;
	int bits = 0;
	int v;
	byte[] byteArray;
	char[] chars;
	static final char[] hexArray = "0123456789ABCDEF".toCharArray();

	BinaryGenerator(Integer bytes, Integer bits){
		if (bits != 0){
			this.bytes = (bytes + bits / 8);
			this.bits = (bits % 8);

			unique = ((bits != 0) && (bytes != 8));
		} else {
			this.bytes = bytes;
			unique = (bytes != 8);
		}
		byteArray = new byte[bytes];
	}

	public String generate(){
		return "Do not instantiate a BinaryGenerator directly";
	}

	public static void setBinaryFormat(String bf){
		format = bf;
	}

	public static Generator get(String generatorClass, Object... arguments) {
		try {
			switch (format){
				case "hex":
					return get(Class.forName("org.rearviewofagenius.iain.data.generators.HexadecimalGenerator"), arguments);
				case "bitstring":
					return get(Class.forName("org.rearviewofagenius.iain.data.generators.BitstringGenerator"), arguments);
				default:
					return get(Class.forName("org.rearviewofagenius.iain.data.generators.OctetGenerator"), arguments);
			}
		} catch (ClassNotFoundException cnfe) {
			System.err.println(generatorClass + " not found");
		}
		return null;
	}

	public String toString(){
		return "Binary Generator";
	}
}
