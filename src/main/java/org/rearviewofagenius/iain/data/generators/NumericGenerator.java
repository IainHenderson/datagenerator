package org.rearviewofagenius.iain.data.generators;

/**
 * Generator for fixed-point numeric data.
 * https://www.vertica.com/docs/9.2.x/HTML/Content/Authoring/SQLReferenceManual/DataTypes/Numeric/NUMERIC.htm?tocpath=SQL%20Reference%20Manual%7CSQL%20Data%20Types%7CNumeric%20Data%20Types%7C_____3
 */
public class NumericGenerator extends Generator {

    /**
     * The total number of significant digits in this numeric.
     */
    private Integer precision = 37;
    /**
     * The maximum number of digits to the right of the decimal point.
     */
    private Integer scale = 0;

    /**
     * @param precision significant digits
     * @param scale digits to the right of the decimal point
     */
    public NumericGenerator(final Integer precision, final Integer scale) {
        if (precision != null) {
            if (precision < 0) {
                this.precision = 0;
            } else if (precision > 1024) {
                this.precision = 1024;
            } else {
                this.precision = precision;
            }
            unique = true;
        }
        if ((scale != null) && (scale > 0)) {
            if ((precision != null) && (scale > precision)) {
                this.scale = precision;
            }
            else {
                this.scale = scale;
            }
            unique = true;
        }
    }

    /**
     * @param precision significant digits
     */
    public NumericGenerator(final Integer precision) {
        this(precision, null);
    }

    /**
     */
    public NumericGenerator() {
        this(37, 0);
    }

    @Override
    public String generate() {
        StringBuilder sb = new StringBuilder();
        int precision = this.precision - this.scale;
        int scale = this.scale;
        if (precision > 0) {
            while (precision > 10) {
                sb.append(nextInt((int) Math.pow(10.0D, 10.0D) - 1));
                precision -= 10;
            }
            sb.append(nextInt((int) Math.pow(10.0D, precision) - 1));
        }
        else {
            sb.append(0);
        }
        if (scale > 0) {
            sb.append(".");
            while (scale > 10) {
                sb.append(nextInt((int) Math.pow(10.0D, 1.0D) - 1));
                scale -= 10;
            }
            sb.append(nextInt((int) Math.pow(10.0D, scale) - 1));
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return "Numeric Generator";
    }
}
