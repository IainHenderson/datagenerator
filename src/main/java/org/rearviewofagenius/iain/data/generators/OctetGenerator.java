package org.rearviewofagenius.iain.data.generators;

public class OctetGenerator extends BinaryGenerator {

	public OctetGenerator(Integer bytes, Integer bits){
	    super(bytes, bits);
	  }
	  
	  public OctetGenerator(Integer bytes){
		  this(bytes, 0);
	  }

	  public OctetGenerator(){
		  this(8);
	  }
		  
	  public String generate()
	  {
	    byteArray = new byte[bytes];
	    nextBytes(byteArray);
	    chars = new char[bytes * 5];
	    for (int j = 0; j < bytes; j++)
	    {
	      v = (byteArray[j] & 0xFF);
	      chars[(j * 5)] = '\\';
	      chars[(j * 5 + 1)] = '\\';
	      chars[(j * 5 + 2)] = hexArray[(v / 64)];
	      chars[(j * 5 + 3)] = hexArray[(v % 64 / 8)];
	      chars[(j * 5 + 4)] = hexArray[(v % 8)];
	    }

	    return new String(chars);
	  }
	  
	  public String toString(){
	    return "Binary Generator";
	  }

}
