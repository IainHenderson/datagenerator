package org.rearviewofagenius.iain.data.generators;

public class HexadecimalGenerator extends BinaryGenerator {

	  public HexadecimalGenerator(Integer bytes, Integer bits){
		    super(bytes, bits);
	  }
	  
	  public HexadecimalGenerator(Integer bytes){
		  this(bytes, 0);
	  }

	  public HexadecimalGenerator(){
		  this(8);
	  }
		  
	  public String generate()
	  {
	    byteArray = new byte[bytes];
	    nextBytes(byteArray);

	    chars = new char[bytes * 2 + 2];
        chars[0] = '0';
        chars[1] = 'x';
        for (int j = 0; j < bytes; j++)
        {
          v = (byteArray[j] & 0xFF);
          chars[(j * 2 + 2)] = hexArray[(v >>> 4)];
          chars[(j * 2 + 3)] = hexArray[(v & 0xF)];
        }
	    return new String(chars);
	  }
	  
	  public String toString(){
	    return "Binary Generator";
	  }

}
