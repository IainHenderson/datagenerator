package org.rearviewofagenius.iain.data.generators;

/**
 * Created by Iain_Henderson on 11/8/16.
 */
public class SocialSecurityNumberGenerator extends Generator {

    public SocialSecurityNumberGenerator() {}

    @Override
    public String generate() {
        int areaNumber = nextInt(998) + 1;
        while(areaNumber == 666) {
            areaNumber = nextInt(998) + 1;
        }
        // fun fact: <area number>-<group number>-<serial number>
        return String.format("%03d-%02d-%04d", areaNumber, nextInt(98) + 1, nextInt(9998) + 1);
    }
}
