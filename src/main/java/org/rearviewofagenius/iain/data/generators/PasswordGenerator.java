package org.rearviewofagenius.iain.data.generators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Password Generator.
 */
public class PasswordGenerator extends StringGenerator {

    private static final int MAX_WORD_COUNT_DEFAULT = 4;
    private static final int MAXIMUM_WORD_LENGTH = 20;
    private static final int MINIMUM_WORD_LENGTH_DEFAULT = 2;
    /**
     * Special characters inserted into passwords.
     */
    public static final String SPECIAL_CHARACTERS = "!@#$%^&*()-+=[]{}<>':;?";
    /*
     * Random Character Constants
     * Passwords must be at lease MINIMUM_CHARACTER_COUNT * 4 characters long
     * They must contain at least MINIMUM_CHARACTER_COUNT of each class of character (uppercase, lowercase, special, and numeric)
     * The MINIMUM_CHARACTER_CHANCE is the minimum probability that a given character class will be used
     * The MAXIMUM_CHARACTER_CHANCE is the maximum probability that a given character class will be used
     * CHARACTER_CHANCE_INTERVAL is the shift in probability as characters of a given class are encountered, allows some room for extra occurrences
     */
    private static final double MINIMUM_CHARACTER_COUNT = 3;
    private static final double MINIMUM_CHARACTER_CHANCE = .02;
    public static final double MINIMUM_PASSWORD_LENGTH = 8;
    private static final double MAXIMUM_CHARACTER_CHANCE = .3;
    private static final double CHARACTER_CHANCE_INTERVAL = (MAXIMUM_CHARACTER_CHANCE - MINIMUM_CHARACTER_CHANCE) / (MINIMUM_CHARACTER_COUNT + 1); // the +1 allows for a more occurrences than the minimum
    private static final Map<Character, Set<Character>> CHARACTER_MAPPING = new HashMap<>();

    static {
        CHARACTER_MAPPING.put('a', new HashSet<>(Arrays.asList('a', 'A', '@', '4')));
        CHARACTER_MAPPING.put('b', new HashSet<>(Arrays.asList('b', 'B')));
        CHARACTER_MAPPING.put('c', new HashSet<>(Arrays.asList('c', 'C', '(')));
        CHARACTER_MAPPING.put('d', new HashSet<>(Arrays.asList('d', 'D')));
        CHARACTER_MAPPING.put('e', new HashSet<>(Arrays.asList('e', 'E', '&', '3')));
        CHARACTER_MAPPING.put('f', new HashSet<>(Arrays.asList('f', 'F')));
        CHARACTER_MAPPING.put('g', new HashSet<>(Arrays.asList('g', 'G')));
        CHARACTER_MAPPING.put('h', new HashSet<>(Arrays.asList('h', 'H', '#')));
        CHARACTER_MAPPING.put('i', new HashSet<>(Arrays.asList('h', 'I')));
        CHARACTER_MAPPING.put('j', new HashSet<>(Arrays.asList('j', 'J')));
        CHARACTER_MAPPING.put('k', new HashSet<>(Arrays.asList('k', 'K')));
        CHARACTER_MAPPING.put('l', new HashSet<>(Arrays.asList('l', 'L')));
        CHARACTER_MAPPING.put('m', new HashSet<>(Arrays.asList('m', 'M')));
        CHARACTER_MAPPING.put('n', new HashSet<>(Arrays.asList('n', 'N')));
        CHARACTER_MAPPING.put('o', new HashSet<>(Arrays.asList('o', 'O', '*', '0')));
        CHARACTER_MAPPING.put('p', new HashSet<>(Arrays.asList('p', 'P')));
        CHARACTER_MAPPING.put('q', new HashSet<>(Arrays.asList('q', 'Q')));
        CHARACTER_MAPPING.put('r', new HashSet<>(Arrays.asList('r', 'R')));
        CHARACTER_MAPPING.put('s', new HashSet<>(Arrays.asList('s', 'S', '$', '5')));
        CHARACTER_MAPPING.put('t', new HashSet<>(Arrays.asList('t', 'T', '7')));
        CHARACTER_MAPPING.put('u', new HashSet<>(Arrays.asList('u', 'U')));
        CHARACTER_MAPPING.put('v', new HashSet<>(Arrays.asList('v', 'V')));
        CHARACTER_MAPPING.put('w', new HashSet<>(Arrays.asList('w', 'W')));
        CHARACTER_MAPPING.put('x', new HashSet<>(Arrays.asList('x', 'X', '%', '4')));
        CHARACTER_MAPPING.put('y', new HashSet<>(Arrays.asList('y', 'Y')));
        CHARACTER_MAPPING.put('z', new HashSet<>(Arrays.asList('z', 'Z', '2')));
        Map<Character, Set<Character>> expandedCharacterMapping = new HashMap<>();
        for (Character c : CHARACTER_MAPPING.keySet()) {
            for (Character c2 : CHARACTER_MAPPING.get(c)) {
                if (!CHARACTER_MAPPING.containsKey(c2)) {
                    expandedCharacterMapping.put(c2, CHARACTER_MAPPING.get(c));
                }
            }
        }
        CHARACTER_MAPPING.putAll(expandedCharacterMapping);
    }

    private final int maxWordCount;
    private final int minWordLength;

    /**
     */
    public PasswordGenerator() {
        this(MAX_WORD_COUNT_DEFAULT);
    }

    /**
     * @param maxWordCount maximum number of individual words
     */
    public PasswordGenerator(final Integer maxWordCount) {
        this(maxWordCount, MINIMUM_WORD_LENGTH_DEFAULT);
    }

    /**
     * @param maxWordCount maximum number of individual words
     * @param wordLength minumum word length
     */
    public PasswordGenerator(final Integer maxWordCount, final Integer wordLength) {
        this(maxWordCount, wordLength, DICTIONARY);
    }

    /**
     * @param file file to read words from
     */
    public PasswordGenerator(final String file) {
        this(MAX_WORD_COUNT_DEFAULT, file);
    }

    /**
     * @param maxWordCount maximum number of individual words
     * @param file file to read words from
     */
    public PasswordGenerator(final Integer maxWordCount, final String file) {
        this(maxWordCount, MINIMUM_WORD_LENGTH_DEFAULT, file);
    }

    /**
     * @param maxWordCount maximum number of individual words
     * @param wordLength minumum word length
     * @param file file to read words from
     */
    public PasswordGenerator(final Integer maxWordCount, final Integer wordLength, final String file) {
        super(file, 0);
        unique = maxWordCount != MAX_WORD_COUNT_DEFAULT || wordLength != MINIMUM_WORD_LENGTH_DEFAULT;
        this.maxWordCount = maxWordCount;
        if(wordLength > MAXIMUM_WORD_LENGTH) {
            minWordLength = MAXIMUM_WORD_LENGTH;
        } else {
            if (wordLength * maxWordCount < MINIMUM_PASSWORD_LENGTH) {
                minWordLength = (int) Math.round(MINIMUM_PASSWORD_LENGTH / maxWordCount);
            }
            else {
                minWordLength = wordLength;
            }
        }
    }

    @Override
    public String generate() {
        if (sb == null) {
            sb = new StringBuilder();
        }
        else {
            sb.setLength(0);
        }
        for (int i = 0; i < maxWordCount; i++) {
            if (.125 > nextDouble()) {
                sb.append(SPECIAL_CHARACTERS.charAt(nextInt(SPECIAL_CHARACTERS.length())));
            }
            if (sb.length() > 0 && maxWordCount > 1) {
                sb.append(" ");
            }
            String next = nextString();
            while (!good(next)) {
                next = nextString();
            }
            sb.append(next);
            if (.125 > nextDouble()) {
                sb.append(nextInt(1000));
            }
        }
        return new Password(sb.toString()).toString();
    }

    // Define the length of each string for a password
    protected boolean good(final String chunk) {
        return chunk.length() >= minWordLength && chunk.length() <= MAXIMUM_WORD_LENGTH;
    }

    private class Password {

        private String password;
        private int upperCase;
        private int lowerCase;
        private int numeric;
        private int special;
        private int spaces;
        private int updateCount;

        /**
         * @param p password
         */
        Password(final String p) {
            password = p;
            count(password);
            while (lowerCase < MINIMUM_CHARACTER_COUNT || upperCase < MINIMUM_CHARACTER_COUNT || numeric < MINIMUM_CHARACTER_COUNT || special < MINIMUM_CHARACTER_COUNT) {
                encode();
            }
            if (verbose) {
                System.err.println(password);
            }
        }

        private void count(final String password) {
            upperCase = 0;
            lowerCase = 0;
            numeric = 0;
            special = 0;
            for (char ch : password.toCharArray()) {
                if (Character.isUpperCase(ch)) {
                    upperCase++;
                }
                else if (Character.isLowerCase(ch)) {
                    lowerCase++;
                }
                else if (Character.isDigit(ch)) {
                    numeric++;
                }
                else if (SPECIAL_CHARACTERS.contains(Character.toString(ch))) {
                    special++;
                }
                else if (ch == ' ') {
                    spaces++;
                }
            }
        }

        private void encode() {
            if(updateCount < 100) {
                // insert numerics and/or special characters around the spaces
                StringBuilder sb = new StringBuilder();
                for (char ch : password.toCharArray()) {
                    if (ch == ' ') {
                        if (chance(numeric)) {
                            sb.append(nextInt(10));
                            numeric++;
                        }
                        else if (chance(special)) {
                            sb.append(SPECIAL_CHARACTERS.charAt(nextInt(SPECIAL_CHARACTERS.length())));
                            special++;
                        }
                    }
                    sb.append(ch);
                }
                char[] modified = sb.toString().toCharArray();
                // build an array of locations in the string
                ArrayList<Integer> locations = new ArrayList<>();
                for (locations.size(); locations.size() < password.length(); ) {
                    locations.add(locations.size());
                }
                int next;
                while (!locations.isEmpty()) {
                    next = nextInt(locations.size());
                    modified[locations.get(next)] = update(modified[locations.get(next)]);
                    locations.remove(next);
                }
                password = new String(modified);
            }
            else {
                if(lowerCase < MINIMUM_CHARACTER_COUNT || upperCase < MINIMUM_CHARACTER_COUNT) {
                    if (lowerCase + upperCase >= MINIMUM_CHARACTER_COUNT * 2) {
                        char[] modified = password.toCharArray();
                        ArrayList<Integer> locations = new ArrayList<>();
                        for (locations.size(); locations.size() < password.length(); ) {
                            locations.add(locations.size());
                        }
                        int next;
                        while (!locations.isEmpty()) {
                            next = nextInt(locations.size());
                            if (Character.isAlphabetic(modified[locations.get(next)])) {
                                if (Character.isUpperCase(modified[locations.get(next)]) && lowerCase < MINIMUM_CHARACTER_COUNT) {
                                    modified[locations.get(next)] = Character.toLowerCase(modified[locations.get(next)]);
                                    upperCase--;
                                    lowerCase++;
                                }
                                if (Character.isLowerCase(modified[locations.get(next)]) && upperCase < MINIMUM_CHARACTER_COUNT) {
                                    modified[locations.get(next)] = Character.toUpperCase(modified[locations.get(next)]);
                                    upperCase++;
                                    lowerCase--;
                                }
                            }
                            locations.remove(next);
                        }
                        password = new String(modified);
                    }
                    else {
                        if (lowerCase < MINIMUM_CHARACTER_COUNT && chance(lowerCase)) {
                            insert((char) nextInt('z'));
                            lowerCase++;
                        }
                        if (upperCase < MINIMUM_CHARACTER_COUNT && chance(upperCase)) {
                            insert(Character.toUpperCase((char) nextInt('z')));
                            upperCase++;
                        }
                    }
                }
                if (numeric < MINIMUM_CHARACTER_COUNT && chance(numeric)) {
                    insert(Integer.toString(nextInt(10)).toCharArray()[0]);
                    numeric++;
                }
                if (special < MINIMUM_CHARACTER_COUNT && chance(special)) {
                    insert(SPECIAL_CHARACTERS.charAt(nextInt(SPECIAL_CHARACTERS.length())));
                    special++;
                }
            }
            updateCount++;
        }

        /**
         * Insert the specified character at a random location.
         * @param character character to insert
         */
        private void insert(final char character) {
            int location = nextInt(password.length());
            if (location > 0) {
                password = password.substring(0, location) + character + password.substring(location);
            }
            else {
                password = character + password;
            }
        }

        /**
         * Checks the odds based upon a specified count.
         *
         * @param count the current count
         * @return if a character of this class should be used
         */
        private boolean chance(final int count) {
            return (MAXIMUM_CHARACTER_CHANCE - CHARACTER_CHANCE_INTERVAL * count < MINIMUM_CHARACTER_CHANCE ? MINIMUM_CHARACTER_CHANCE : MAXIMUM_CHARACTER_CHANCE - CHARACTER_CHANCE_INTERVAL * count) > nextDouble();
        }

        public String toString() {
            return password;
        }

        private Character update(final Character character) {
            Set<Character> randomized = CHARACTER_MAPPING.getOrDefault(character, Collections.emptySet());
            for (Character newCharacter : randomized) {
                if (Character.isDigit(newCharacter) && chance(numeric)) {
                    decrement(character);
                    numeric++;
                    return newCharacter;
                }
                else if (SPECIAL_CHARACTERS.contains(newCharacter.toString()) && chance(special)) {
                    decrement(character);
                    special++;
                    return newCharacter;
                }
                else if (Character.isLowerCase(newCharacter) && chance(lowerCase)) {
                    decrement(character);
                    lowerCase++;
                    return newCharacter;
                }
                else if (Character.isUpperCase(newCharacter) && chance(upperCase)) {
                    decrement(character);
                    upperCase++;
                    return newCharacter;
                }
            }
            return character;
        }

        private void decrement(final Character c) {
            if (Character.isDigit(c)) {
                numeric--;
            }
            else if (SPECIAL_CHARACTERS.contains(Character.toString(c))) {
                special--;
            }
            else if (Character.isLowerCase(c)) {
                lowerCase--;
            }
            else if (Character.isUpperCase(c)) {
                upperCase--;
            }
        }
    }
}
