package org.rearviewofagenius.iain.data.generators;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Abstract Generator implementation.
 */
public abstract class Generator {

    static final int QUEUE_DEPTH = 50;
    static final long TIMEOUT = 5;
    static final HashMap<Class<?>, Generator> GENERATORS = new HashMap<Class<?>, Generator>();
    static final ExecutorService EXECUTOR_SERVICE = Executors.newWorkStealingPool();
    static boolean verbose;
    private final LinkedBlockingDeque<String> deque = new LinkedBlockingDeque<>(QUEUE_DEPTH);
    protected boolean unique;

    /**
     * Default Constructor.
     */
    protected Generator() {
    }

    public static Generator get(final String generatorClass, final Object... arguments) {
        try {
            return get(Class.forName(generatorClass), arguments);
        }
        catch (ClassNotFoundException cnfe) {
            System.err.println(generatorClass + " not found");
        }
        return null;
    }

    public static Generator get(final Class<?> generatorClass, final Object... arguments) {
        Generator generator = null;
        Class<?>[] argumentClasses = new Class[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            argumentClasses[i] = arguments[i].getClass();
        }
        try {
            generator = (Generator) generatorClass.getConstructor(argumentClasses).newInstance(arguments);
            if (!generator.unique) {
                if (GENERATORS.containsKey(generatorClass)) {
                    generator = (Generator) GENERATORS.get(generatorClass);
                }
                else {
                    GENERATORS.put(generator.getClass(), generator);
                }
            }
        }
        catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        }
        return generator;
    }

    public static void shutdownNow() {
        EXECUTOR_SERVICE.shutdown();
        for (Runnable r : EXECUTOR_SERVICE.shutdownNow()) {
            System.out.print(r.toString() + " is still running");
        }
    }

    public static void setVerbose(final boolean v) {
        verbose = v;
    }

    public static boolean nextBoolean() {
        return ThreadLocalRandom.current().nextBoolean();
    }

    public static void nextBytes(final byte[] bytes) {
        ThreadLocalRandom.current().nextBytes(bytes);
    }

    public static double nextDouble() {
        return ThreadLocalRandom.current().nextDouble();
    }

    public static float nextFloat() {
        return ThreadLocalRandom.current().nextFloat();
    }

    public static double nextGaussian() {
        return ThreadLocalRandom.current().nextGaussian();
    }

    public static int nextInt() {
        return ThreadLocalRandom.current().nextInt();
    }

    public static int nextInt(final int bound) {
        return ThreadLocalRandom.current().nextInt(bound);
    }

    public static long nextLong() {
        return ThreadLocalRandom.current().nextLong();
    }

    public static long nextLong(final long bound) {
        return ThreadLocalRandom.current().nextLong(bound);
    }

    public static long nextLong(final long origin, final long bound) {
        return ThreadLocalRandom.current().nextLong(origin, bound);
    }

    public String next() throws InterruptedException {
        EXECUTOR_SERVICE.execute(this::put);
        return take();
    }

    protected String take() throws InterruptedException {
        return deque.take();
    }

    protected void put() {
        try {
            deque.put(generate());
        }
        catch (InterruptedException ie) {
            System.err.println("Failed to generate ");
        }
    }

    public abstract String generate();

    public void finish() {
    }

    public String toString() {
        return "Generator";
    }
}
