package org.rearviewofagenius.iain.data.generators;

import java.util.Date;
import java.text.SimpleDateFormat;

public class DateTimeGenerator extends Generator {

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
	private Long minimum = Long.valueOf(-2682329400000L);
	private Long maximum = Long.valueOf(5364658800000L);

	public DateTimeGenerator(SimpleDateFormat sdf, Long minimum, Long maximum) {
		unique = false;
		if ((sdf != null) && (!sdf.toPattern().isEmpty())) {
			this.sdf = sdf;
			unique = true;
		}
		if (maximum != null) {
			if (this.maximum != maximum) {
				this.maximum = maximum;
				unique = true;
			}
		}
		if (minimum != null) {
			if (this.minimum != minimum) {
				this.minimum = minimum;
				unique = true;
			}
		}
		if(this.minimum > this.maximum){
			Long temp = this.maximum;
			this.maximum = this.minimum;
			this.minimum = temp;
		}
	}

	public DateTimeGenerator() {
		this("");
	}

	public DateTimeGenerator(String format) {
		this( format, (Long)null, (Long)null);
	}

	public DateTimeGenerator(Date range)
	{
		this("", new Date(-1L * Math.abs(range.getTime())), new Date(Math.abs(range.getTime())));
	}

	public DateTimeGenerator(Long minimum, Long maximum)
	{
		this("", minimum, maximum);
	}

	public DateTimeGenerator(Date minimum, Date maximum)
	{
		this("", minimum, maximum);
	}

	public DateTimeGenerator(String format, Date range) {
		this(format, -1L * Math.abs(range.getTime()), Math.abs(range.getTime()));
	}

	public DateTimeGenerator(String format, Long minimum, Long maximum) {
		this(new SimpleDateFormat(format), minimum, maximum);
	}

	public DateTimeGenerator(String format, Date minimum, Date maximum) {
		this(new SimpleDateFormat(format), minimum, maximum);
	}

	public DateTimeGenerator(SimpleDateFormat sdf, Date minimum, Date maximum) {
		this(sdf, minimum.getTime(), maximum.getTime());
	}

	public String generate() {
		return sdf.format(new Date(nextLong(minimum, maximum)));
	}
}
