package org.rearviewofagenius.iain.data.generators;

/**
 * Generator for IEEE-754 double precision float.
 * https://www.vertica.com/docs/9.2.x/HTML/Content/Authoring/SQLReferenceManual/DataTypes/Numeric/DOUBLEPRECISIONFLOAT.htm?tocpath=SQL%20Reference%20Manual%7CSQL%20Data%20Types%7CNumeric%20Data%20Types%7C_____1
 */
public class FloatGenerator extends Generator {

    private Integer precision = 54;

    /**
     */
    public FloatGenerator() {
        this(54);
    }

    /**
     * @param precision Bits of precision.
     */
    public FloatGenerator(final Integer precision) {
        if ((precision != null) && !this.precision.equals(precision)) {
            this.precision = precision;
            unique = true;
        }
    }

    //TODO: Implement something to enforce precision
    @Override
    public String generate() {
        return Double.toString(nextDouble());
    }

    @Override
    public String toString() {
        return "Float Generator";
    }
}
