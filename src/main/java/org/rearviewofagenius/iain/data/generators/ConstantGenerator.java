package org.rearviewofagenius.iain.data.generators;

public class ConstantGenerator extends Generator {
  String constant = "";
  
  public ConstantGenerator(String constant){
    this.constant = constant;
    unique = true;
  }
  
  public String generate(){
    return constant;
  }
  
  public String toString(){
    return constant + " Generator";
  }

  @Override
  public String next() throws InterruptedException {
    return constant;
  }

    @Override
    protected void put(){}
}
