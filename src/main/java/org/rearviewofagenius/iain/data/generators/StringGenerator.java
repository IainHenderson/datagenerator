package org.rearviewofagenius.iain.data.generators;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class StringGenerator extends Generator {
    static final String DICTIONARY = "/usr/share/dict/words";
    String dictionary = "/usr/share/dict/words";
	String file = "";
	RandomAccessFile reader;
	ArrayList<Long> lines = null;
	static ConcurrentHashMap<String, ArrayList<Long>> fileLines = new ConcurrentHashMap<String, ArrayList<Long>>();
	int length = 0;
	StringBuilder sb = null;

	public StringGenerator() {
        this(DICTIONARY, 0);
		unique = false;
	}

	public StringGenerator(final Integer length) {
		this(DICTIONARY, length);
	}

	public StringGenerator(final String file) {
		this(file, 0);
	}

	public StringGenerator(final String sourceFile, final Integer length) {
        if (sourceFile.equalsIgnoreCase(dictionary())) {
            file = dictionary();
            unique = length > 0;
        } else {
            file = sourceFile;
            unique = true;
        }
        try {
            reader = new RandomAccessFile(file, "r");
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        }

        EXECUTOR_SERVICE.submit(this::indexFile);

		if (length > 0) {
			this.length = length;
		}
	}

	public String generate() {
		return length > 0 ? nextString(length) : nextString();
	}

	public void finish() {
		try	{
			reader.close();
		} catch (IOException ioe) {
			System.err.println("Error closing file: " + file);
		}
	}

	public String nextString() {
		try	{
			if (lines != null) {
				reader.seek(((Long)lines.get(nextInt(lines.size()))).longValue());
			} else {
				long position = Math.abs(nextLong() % reader.length());
				boolean search = position > 0L;
				reader.seek(position);
				while (search) {
					if (position > 0L) {
						reader.seek(position - 1L);
						reader.readLine();
						if (reader.getFilePointer() > position) {
							position -= 1L;
						} else {
							search = false;
						}
					} else {
						search = false;
					}
				}
				reader.seek(position);
				if ((fileLines.containsKey(file)) && !fileLines.get(file).isEmpty()) {
					lines = fileLines.get(file);
				}
			}
			return reader.readLine();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return "";
	}

	public String nextString(final int length) {
		if(sb == null){
			sb = new StringBuilder();
		} else {
			sb.setLength(0);
		}
		sb.append(nextString());
		while (sb.length() < length) {
			sb.append(" ");
			sb.append(nextString());
		}
		return sb.substring(0, length);
	}

	private void indexFile() {
		// Uses the concurrent nature of fileLines to synchronize this "check"
		ArrayList<Long> existing = fileLines.putIfAbsent(file, new ArrayList<Long>());

		if (existing == null) {
			ArrayList<Long> newLines = new ArrayList<Long>();
			long pointer = 0L;
            try {
                RandomAccessFile fileReader = new RandomAccessFile(file, "r");
                String line = fileReader.readLine();
                while (line != null) {
                    if (good(line)) {
                        newLines.add(pointer);
                    }
                    pointer = fileReader.getFilePointer();
                    line = fileReader.readLine();
                }
                fileLines.put(file, newLines);
                fileReader.close();
            } catch (IOException ioe){
                ioe.printStackTrace();
            }
		}
	}
	
	protected boolean good(final String line){
		return !line.isEmpty();
	}

	public String toString() {
		return "String Generator";
	}

	public String dictionary() {
		// check the full path
		if (!new File(dictionary).exists()) {
			dictionary = new File(dictionary).getName();
		}
		// check just the file ending
		if (!new File(dictionary).exists()) {
			try {
				// write out the file from the jar if it doesn't exist
				File dictionaryFile = File.createTempFile("words", ".tmp");
				dictionaryFile.deleteOnExit();
				FileOutputStream output = new FileOutputStream(dictionaryFile);
				JarFile jf = new JarFile(new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI()));

				InputStream input = jf.getInputStream(new JarEntry(dictionary));

				int read = 0;
				byte[] bytes = new byte['Ѐ'];
				while ((read = input.read(bytes)) != -1) {
					output.write(bytes, 0, read);
				}
				output.close();
				input.close();
				jf.close();
				dictionary = "words.tmp";
			} catch (URISyntaxException | IOException exception) {
				exception.printStackTrace();
			}
        }
		return dictionary;
	}
}
