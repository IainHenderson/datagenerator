package org.rearviewofagenius.iain.data.generators;

import java.util.Iterator;
import java.util.concurrent.ThreadLocalRandom;

public class IntegerGenerator extends Generator {

	  long maximum = Long.MAX_VALUE;
	  long minimum = Long.MIN_VALUE;
	  
	  public IntegerGenerator(Long minimum, Long maximum) {
	    if (minimum != null && maximum != null && minimum > maximum) {
	      Long temp = minimum;
	      minimum = maximum;
	      maximum = temp;
	    }
	    if ((minimum != null) && (minimum > Long.MIN_VALUE)) {
	      this.minimum = minimum;
	      unique = true;
	    }
	    if ((maximum != null) && (maximum < Long.MAX_VALUE)) {
	      this.maximum = maximum;
	      unique = true;
	    }
	  }
	  
	  public IntegerGenerator(Long maximum) {
		  this(0L, maximum);
	  }
	  
	  public IntegerGenerator() {
	    this(Long.MAX_VALUE);
	  }
	  
	  public String generate() {
	    if (unique) {
	      return Long.toString(nextLong(minimum, maximum));
	    }
	    return Long.toString(nextLong());
	  }
	  
	  public String toString() {
	    return "Integer Generator";
	  }
}
