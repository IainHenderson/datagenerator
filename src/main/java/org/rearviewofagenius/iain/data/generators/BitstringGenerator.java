package org.rearviewofagenius.iain.data.generators;

public class BitstringGenerator extends BinaryGenerator {
    public BitstringGenerator(Integer bytes, Integer bits){
		    super(bytes, bits);
	  }
	  
	  public BitstringGenerator(Integer bytes){
		  this(bytes, 0);
	  }

	  public BitstringGenerator(){
		  this(8);
	  }
		  
	  public String generate() {
	    byteArray = new byte[bytes];
	    nextBytes(byteArray);
        chars = new char[bytes * 8 + bits];
        for (int j = 0; j < bytes; j++)
        {
          v = (byteArray[j] & 0xFF);
          chars[(j * 8)] = hexArray[(v / 128)];
          chars[(j * 8 + 1)] = hexArray[(v % 128 / 64)];
          chars[(j * 8 + 2)] = hexArray[(v % 64 / 32)];
          chars[(j * 8 + 3)] = hexArray[(v % 32 / 16)];
          chars[(j * 8 + 4)] = hexArray[(v % 16 / 8)];
          chars[(j * 8 + 5)] = hexArray[(v % 8 / 4)];
          chars[(j * 8 + 6)] = hexArray[(v % 4 / 2)];
          chars[(j * 8 + 7)] = hexArray[(v % 2)];
        }
        for (int k = bits; k > 0; k--){
			chars[chars.length - k] = nextBoolean() ? '0' : '1';
		}

	    return new String(chars);
	  }
	  
	  public String toString(){
	    return "Binary Generator";
	  }

}
