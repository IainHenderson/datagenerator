/**
 * Order of these enumerations matters as the DataGenerator will attempt to match them in the order they appear.
 * Hence, if a less specific pattern comes first, it could steal what should be a more specific pattern.
 * 
 * If adding Formats to this list, keep CONSTANT last
 */
package org.rearviewofagenius.iain.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.rearviewofagenius.iain.data.generators.BinaryGenerator;
import org.rearviewofagenius.iain.data.generators.Generator;

/**
 *
 * @author Iain_Henderson Created: May 19, 2015
 */
public enum Format {

    //VARBINARY_LENGTH("VARBINARY(length)",
    //    "(.*?)(?:LONG )?VARBINARY\\(([0-9]+)\\)(.*)", 
    //    "org.rearviewofagenius.iain.data.generators.BinaryGenerator",
    //    m -> new Object[]{Integer.valueOf(m.group(2)), 0}),
    /** */
    BINARY_BITS("BINARY(<length> Bits)", "(.*?)BINARY\\(([0-9]+) [Bb][Ii][Tt][Ss]\\)(.*)", "org.rearviewofagenius.iain.data.generators.BinaryGenerator", m -> new Object[] { 0, Integer.valueOf(m.group(2)) }),
    /** */
    BINARY_LENGTH("BINARY(<length>)", "(.*?)(?:LONG )?(?:VAR)?BINARY\\(([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.BinaryGenerator", m -> new Object[] { Integer.valueOf(m.group(2)), 0 }),
    //VARBINARY("VARBINARY","(.*?)(?:LONG )?BINARY(.*)","org.rearviewofagenius.iain.data.generators.BinaryGenerator",
    //    m -> new Object[]{}),
    /** */
    BINARY_VARYING("BINARY VARYING", "(.*?)BINARY VARYING(.*)", "org.rearviewofagenius.iain.data.generators.BinaryGenerator", m -> new Object[] {}),
    /** */
    BINARY("BINARY", "(.*?)(?:LONG )?(?:VAR)?BINARY(.*)", "org.rearviewofagenius.iain.data.generators.BinaryGenerator", m -> new Object[] {}),
    /** */
    BYTEA("BYTEA", "(.*?)BYTEA(.*)", "org.rearviewofagenius.iain.data.generators.BinaryGenerator", m -> new Object[] {}),
    /** */
    RAW("RAW", "(.*?)RAW(.*)", "org.rearviewofagenius.iain.data.generators.BinaryGenerator", m -> new Object[] {}),
    /** */
    BOOLEAN("BOOLEAN", "(.*?)BOOLEAN(.*)", "org.rearviewofagenius.iain.data.generators.BooleanGenerator", m -> new Object[] {}),
    /** */
    BOOL("BOOL", "(.*?)BOOL(.*)", "org.rearviewofagenius.iain.data.generators.BooleanGenerator", m -> new Object[] {}),
    /** */
    CHARACTER_VARYING("CHARACTER VARYING", "(.*?)CHARACTER VARYING(.*)", "org.rearviewofagenius.iain.data.generators.StringGenerator", m -> new Object[] {}),
    /** */
    CHARACTER("CHARACTER", "(.*?)CHARACTER(.*)", "org.rearviewofagenius.iain.data.generators.StringGenerator", m -> new Object[] {}),
    /** */
    CHAR_LENGTH("CHAR(<length>)", "(.*?)(?:LONG )?(?:VAR)?CHAR\\(([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.StringGenerator", m -> new Object[] { Integer.valueOf(m.group(2)) }),
    /** */
    CHAR("CHAR", "(.*?)(?:LONG )?(?:VAR)?CHAR(.*)", "org.rearviewofagenius.iain.data.generators.StringGenerator", m -> new Object[] {}),
    /** */
    INTERVALYM("INTERVALYM", "(.*?)INTERVALYM(.*)", "org.rearviewofagenius.iain.data.generators.IntervalYMGenerator", m -> new Object[] {}),
    /** */
    INTERVAL("INTERVAL", "(.*?)INTERVAL(.*)", "org.rearviewofagenius.iain.data.generators.IntervalGenerator", m -> new Object[] {}),
    /** */
    DOUBLE_PRECISION("DOUBLE PRECISION", "(.*?)DOUBLE PRECISION(.*)", "org.rearviewofagenius.iain.data.generators.FloatGenerator", m -> new Object[] {}),
    /** */
    REAL("REAL", "(.*?)REAL(.*)", "org.rearviewofagenius.iain.data.generators.FloatGenerator", m -> new Object[] {}),
    /** */
    FLOAT_PRECISION("FLOAT(<precision>)", "(.*?)FLOAT\\(([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.FloatGenerator", m -> new Object[] {}),
    /** */
    FLOAT8("FLOAT8", "(.*?)FLOAT8(.*)", "org.rearviewofagenius.iain.data.generators.FloatGenerator", m -> new Object[] {}),
    /** */
    FLOAT("FLOAT", "(.*?)FLOAT(.*)", "org.rearviewofagenius.iain.data.generators.FloatGenerator", m -> new Object[] {}),
    /** */
    INTEGER_MINIMUM_MAXIMUM("INTEGER(<minimum>, <maximum>)", "(.*?)INTEGER\\((-?[0-9]+),\\s*(-?[0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.IntegerGenerator", m -> new Object[] { Long.valueOf(m.group(2)), Long.valueOf(m.group(3)) }),
    /** */
    INTEGER_MAXIMUM("INTEGER(<maximum>)", "(.*?)INTEGER\\((-?[0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.IntegerGenerator", m -> new Object[] { Long.valueOf(m.group(2)) }),
    /** */
    INTEGER("INTEGER", "(.*?)INTEGER(.*)", "org.rearviewofagenius.iain.data.generators.IntegerGenerator", m -> new Object[] {}),
    /** */
    BIGINT("BIGINT", "(.*?)BIGINT(.*)", "org.rearviewofagenius.iain.data.generators.IntegerGenerator", m -> new Object[] {}),
    /** */
    INT8("INT8", "(.*?)INT8(.*)", "org.rearviewofagenius.iain.data.generators.IntegerGenerator", m -> new Object[] {}),
    /** */
    SMALLINT("SMALLINT", "(.*?)SMALLINT(.*)", "org.rearviewofagenius.iain.data.generators.IntegerGenerator", m -> new Object[] {}),
    /** */
    TINYINT("TINYINT", "(.*?)TINYINT(.*)", "org.rearviewofagenius.iain.data.generators.IntegerGenerator", m -> new Object[] {}),
    /** */
    INT("INT", "(.*?)INT(.*)", "org.rearviewofagenius.iain.data.generators.IntegerGenerator", m -> new Object[] {}),
    /** */
    NUMERIC_PRECISION_SCALE("NUMERIC(<precision>, <scale>)", "(.*?)NUMERIC\\(([0-9]+),\\s*([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.NumericGenerator", m -> new Object[] {Integer.valueOf(m.group(2)), Integer.valueOf(m.group(3))}),
    /** */
    NUMERIC_PRECISION("NUMERIC(<precision>)", "(.*?)NUMERIC\\(([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.NumericGenerator", m -> new Object[] {}),
    /** */
    NUMERIC("NUMERIC", "(.*?)NUMERIC(.*)", "org.rearviewofagenius.iain.data.generators.NumericGenerator", m -> new Object[] {}),
    /** */
    DECIMAL("DECIMAL", "(.*?)DECIMAL(.*)", "org.rearviewofagenius.iain.data.generators.NumericGenerator", m -> new Object[] {}),
    /** */
    SSN("SOCIAL SERCURITY NUMBER","(.*?)(?:SSN|SOCIAL(?: SECURITY NUMBER)?)(.*)", "org.rearviewofagenius.iain.data.generators.SocialSecurityNumberGenerator", m -> new Object[] {}),
    /** */
    NUMBER("NUMBER", "(.*?)NUMBER(.*)", "org.rearviewofagenius.iain.data.generators.NumericGenerator", m -> new Object[] {}),
    /** */
    MONEY("MONEY", "(.*?)MONEY(.*)", "org.rearviewofagenius.iain.data.generators.NumericGenerator", m -> new Object[] {}),
    /** */
    FOREIGNKEY("FOREIGN KEY", "(.*?)FOREIGN ?KEY\\((.*?)\\)(.*)", "org.rearviewofagenius.iain.data.generators.StringGenerator", m -> new Object[] {}),
    /** */
    TELEPHONE("TELEPHONE", "(.*?)TELEPHONE(.*)", "org.rearviewofagenius.iain.data.generators.TelephoneNumberGenerator", m -> new Object[] {}),
    /** */
    PASSWORD_COUNT_LENGTH_FILE("PASSWORD(<count>, <length>, <file>)", "(.*?)PASSWORD\\(([0-9]+),\\s*([0-9]+),\\s*(.*)\\)(.*)", "org.rearviewofagenius.iain.data.generators.PasswordGenerator", m -> new Object[] { Integer.valueOf(m.group(2)), Integer.valueOf(m.group(3)), m.group(4) }),
    /** */
    PASSWORD_COUNT_LENGTH("PASSWORD(<count>, <length>)", "(.*?)PASSWORD\\(([0-9]+),\\s*([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.PasswordGenerator", m -> new Object[] { Integer.valueOf(m.group(2)), Integer.valueOf(m.group(3)) }),
    /** */
    PASSWORD_COUNT_FILE("PASSWORD(<count>, <file>)", "(.*?)PASSWORD\\(([0-9]+),\\s*(.*)\\)(.*)", "org.rearviewofagenius.iain.data.generators.PasswordGenerator", m -> new Object[] { Integer.valueOf(m.group(2)), m.group(3) }),
    /** */
    PASSWORD_COUNT("PASSWORD(<count>)", "(.*?)PASSWORD\\(([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.PasswordGenerator", m -> new Object[] { Integer.valueOf(m.group(2)) }),
    /** */
    PASSWORD_FILE("PASSWORD(<file>)", "(.*?)PASSWORD\\((.*?)\\)(.*)", "org.rearviewofagenius.iain.data.generators.PasswordGenerator", m -> new Object[] { m.group(2) }),
    /** */
    PASSWORD("PASSWORD", "(.*?)PASSWORD(.*)", "org.rearviewofagenius.iain.data.generators.PasswordGenerator", m -> new Object[] {}),
    /** */
    DATETIME_FORMAT_MINIMUM_MAXIMUM_LONG("DATETIME(<Simple Date Format>,<minimum as a long>,<maximum as a long>)", "(.*?)DATETIME\\(([0-9]+),\\s*([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {m.group(2), Long.parseLong(m.group(3)), Long.parseLong(m.group(4))}),
    /** */
    DATETIME_FORMAT_MINIMUM_MAXIMUM("DATETIME(<Simple Date Format>,<minimum>,<maximum>)", "(.*?)DATETIME\\((.+),\\s*(.+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {m.group(2), parseDate(m.group(3)), parseDate(m.group(4))}),
    /** */
    DATETIME_MINIMUM_MAXIMUM_LONG("DATETIME(<minimum as a long>,<maximum as a long>)", "(.*?)DATETIME\\(([0-9]+),\\s*([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {Long.parseLong(m.group(2)), Long.parseLong(m.group(3))}),
    /** */
    DATETIME_MINIMUM_MAXIMUM("DATETIME(<minimum>,<maximum>)", "(.*?)DATETIME\\((.+),\\s*(.+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {parseDate(m.group(2)), parseDate(m.group(3))}),
    /** */
    DATETIME_FORMAT("DATETIME(<Simple Date Format>)", "(.*?)DATETIME\\((.+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] { m.group(2)}),
    /** */
    DATETIME("DATETIME", "(.*?)DATETIME(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {}),
    /** */
    DATE_FORMAT_MINIMUM_MAXIMUM_LONG("DATE(<Simple Date Format>,<minimum as a long>,<maximum as a long>)", "(.*?)DATE\\((.+),\\s*([0-9]+),\\s*([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {m.group(2), Long.parseLong(m.group(3)), Long.parseLong(m.group(4))}),
    /** */
    DATE_FORMAT_MINIMUM_MAXIMUM("DATE(<Simple Date Format>,<minimum>,<maximum>)", "(.*?)DATE\\((.+),\\s*(.+),\\s*(.+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {m.group(2), parseDate(m.group(3)), parseDate(m.group(4))}),
    /** */
    DATE_MINIMUM_MAXIMUM_LONG("DATE(<minimum as a long>,<maximum as a long>)", "(.*?)DATE\\(([0-9]+),\\s*([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {"yyyy-MM-dd", Long.parseLong(m.group(2)), Long.parseLong(m.group(3))}),
    /** */
    DATE_MINIMUM_MAXIMUM("DATE(<minimum>,<maximum>)", "(.*?)DATE\\((.+),\\s*(.*)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {"yyyy-MM-dd", parseDate(m.group(2)), parseDate(m.group(3))}),
    /** */
    DATE_FORMAT("DATE(<Simple Date Format>)", "(.*?)DATE\\((.+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {m.group(2)}),
    /** */
    DATE("DATE", "(.*?)DATE(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {"yyyy-MM-dd"}),
    /** */
    TIME_FORMAT_MINIMUM_MAXIMUM_LONG("DATE(<Simple Date Format>,<minimum as a long>,<maximum as a long>)", "(.*?)DATE\\((.+),\\s*([0-9]+),\\s*([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {m.group(2), Long.parseLong(m.group(3)), Long.parseLong(m.group(4))}),
    /** */
    TIME_FORMAT_MINIMUM_MAXIMUM("DATE(<Simple Date Format>,<minimum>,<maximum>)", "(.*?)DATE\\((.+),\\s*(.+),\\s*(.+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {m.group(2), parseDate(m.group(3)), parseDate(m.group(4))}),
    /** */
    TIME_MINIMUM_MAXIMUM_LONG("TIME(<minimum as a long>,<maximum as a long>)", "(.*?)TIME\\(([0-9]+),\\s*([0-9]+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {"kk:mm:ss", Long.parseLong(m.group(2)), Long.parseLong(m.group(3))}),
    /** */
    TIME_MINIMUM_MAXIMUM("TIME(<minimum>,<maximum>)", "(.*?)TIME\\((.+),\\s*(.*)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {"kk:mm:ss", parseDate(m.group(2)), parseDate(m.group(3))}),
    /** */
    TIME_FORMAT("TIME(<Simple Date Format>)", "(.*?)TIME\\((.+)\\)(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {m.group(2)}),
    /** */
    TIME("TIME", "(.*?)TIME(.*)", "org.rearviewofagenius.iain.data.generators.DateTimeGenerator", m -> new Object[] {"kk:mm:ss"}),
    /** */
    CONSTANT("CONSTANT", "(.*?)", "org.rearviewofagenius.iain.data.generators.ConstantGenerator", m -> new Object[] { m.group() });

    private final String description;
    private final String generator;
    private final Pattern pattern;
    private final Function<Matcher, Object[]> argumentLogic;

    static final DateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("YYYY-MM-DD");

    /**
     * @param newDescription Human readable pattern
     * @param newPattern RegEx pattern
     * @param generatorClassName class that produces this pattern
     * @param argument Function to generate arguments
     */
    Format(final String newDescription, final String newPattern, final String generatorClassName,
        final Function<Matcher, Object[]> argument) {
        description = newDescription;
        generator = generatorClassName;
        pattern = Pattern.compile(newPattern);
        argumentLogic = argument;
    }

    @Override
    public String toString() {
        return description;
    }

    /**
     * @param row template to be matched to this pattern
     * @return Matcher for this pattern
     */
    public Matcher matcher(final String row) {
        return pattern.matcher(row);
    }

    /**
     * @param matcher matcher to produce arguments
     * @return list of arguments
     */
    public Object[] arguments(final Matcher matcher) {
        return argumentLogic.apply(matcher);
    }

    /**
     * @param row row template
     * @return true if this template
     */
    public boolean matches(final String row) {
        return pattern.matcher(row).matches();
    }

    /**
     * @param row row template
     * @return the queue that this class fills
     */
    public Generator generator(final String row) {
        Matcher matcher = matcher(row);
        matcher.matches();
        return generator(matcher);
    }

    /**
     * @param matcher matcher to produce arguments
     * @return the queue that this class fills
     */
    public Generator generator(final Matcher matcher) {
        return generator(arguments(matcher));
    }

    /**
     * @param arguments arguments for class constructor
     * @return the queue that this class fills
     */
    public Generator generator(final Object... arguments) {
        if (generator.endsWith("BinaryGenerator")) {
            return BinaryGenerator.get(generator, arguments);
        }
        return Generator.get(generator, arguments);
    }

    /**
     * Parse date using the default format.
     * @param date date to be parsed
     * @return a date object or now if that cannot be derived
     */
    static Date parseDate(final String date){
        try{
            return DEFAULT_DATE_FORMAT.parse(date);
        } catch (ParseException pe){
            return new Date();
        }
    }
}
