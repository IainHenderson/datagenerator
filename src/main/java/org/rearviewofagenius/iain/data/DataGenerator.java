package org.rearviewofagenius.iain.data;

import java.util.ArrayList;
import java.util.regex.Matcher;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.MutuallyExclusiveGroup;
import net.sourceforge.argparse4j.inf.Namespace;

import org.rearviewofagenius.iain.data.generators.BinaryGenerator;
import org.rearviewofagenius.iain.data.generators.Generator;



/**
 *
 * @author Iain_Henderson Created: May 19, 2016
 */
public class DataGenerator {

    private static boolean verbose;
    /**
     * @param args command line arguments
     */
    public static void main(final String[] args) {

        final ArgumentParser parser = ArgumentParsers.newArgumentParser("DataGenerator")
                .defaultHelp(true)
                .description("Multithreaded data generator that parses a pattern and outputs random data based upon that.  All input dates should be YYYY-MM-DD")
                .epilog(getFormatSpecifiersMessage());
        parser.addArgument("-v", "--verbose")
                .action(Arguments.count())
                .required(false)
                .help("Verbose mode.  Multiple -v options increase the verbosity.");

        final MutuallyExclusiveGroup binaryGroup = parser.addMutuallyExclusiveGroup();
        binaryGroup.addArgument("-H", "--hexadecimal", "--hex").dest("binaryFormat")
                .action(Arguments.storeConst())
                .setConst("hex").setDefault("octet")
                .required(false)
                .help("Outputs binary output as hexadecimal");
        binaryGroup.addArgument( "-b", "--bitstring", "--bit-string", "--bit")
                .dest("binaryFormat")
                .action(Arguments.storeConst())
                .setConst("bitstring")
                .setDefault("octet")
                .required(false)
                .help("Outputs binary output as bitstrings");

        parser.addArgument("row")
                .metavar("PATTERN")
                .required(true);
        parser.addArgument("rowCount")
                .nargs("?")
                .type(Integer.class)
                .metavar("ROW COUNT")
                .setDefault(10);

        final Namespace arguments = parser.parseArgsOrFail(args);

        final long time = System.currentTimeMillis();
        DataGenerator.binaryFormat(arguments.get("binaryFormat"));
        DataGenerator.verbose(arguments.getInt("verbose") > 0);
        DataGenerator.generate(arguments.getString("row"), arguments.getInt("rowCount"));
        if (arguments.getInt("verbose") > 0) {
            System.err.println(System.currentTimeMillis() - time + "ms");
        }
    }

    /**
     * @param bf format for this generator
     */
    private static void binaryFormat(final String bf) {
        BinaryGenerator.setBinaryFormat(bf);
    }

    /**
     * @param v verbose setting for this generator
     */
    private static void verbose(final boolean v) {
        DataGenerator.verbose = v;
        Generator.setVerbose(v);
    }

    /**
     * @param row row template for this generator
     * @param rowCount row count for this generator
     */
    public static void generate(final String row, final Integer rowCount) {
        write(parseFormat(row,null), rowCount);
    }

    /**
     * @param pieces queues containing the pieces to compose these rows
     * @param rowCount number of rows to output
     */
    private static void write(final ArrayList<Generator> pieces, final Integer rowCount) {
        int currentRowCount = rowCount;
        do {
            for (final Generator piece : pieces) {
                try {
                    System.out.print(piece.next());
                }
                catch (final InterruptedException ignored) {
                }
            }
            System.out.println();
            currentRowCount--;
        } while (currentRowCount > 0);
        Generator.shutdownNow();
    }

    /**
     * @param row row template to generate data queues
     * @param index index within template
     * @return list of queues to produce data for this template
     */
    private static ArrayList<Generator> parseFormat(final String row, final Integer index) {
        if (verbose) {
            System.err.println("row: " + row);
            System.err.println("index: " + index);
            if (index == null) {
                System.err.println(row);
            }
            else if (index < Format.values().length) {
                System.err.println(Format.values()[index] + ": " + row);
            }
            else {
                System.err.println(row + " CONSTANT");
            }
        }
        final ArrayList<Generator> pieces = new ArrayList<>();
        if (row != null && !row.isEmpty()) {
            // initial call of this
            if (index == null) {
                pieces.addAll(parseFormat(row, 0));
            } else {
                final Format format = Format.values()[index];
                final Matcher matcher = format.matcher(row);
                if (verbose) {
                    System.err.println(matcher.pattern().toString() + " has " + matcher.groupCount() + " groups");
                }

                if (matcher.matches()) {
                    if (verbose) {
                        for (int groupIndex = 1; groupIndex <= matcher.groupCount(); groupIndex++) {
                            System.err.println(matcher.group(groupIndex));
                        }
                    }

                    // constant SHOULD catch the entire row and leave nothing else
                    if(!format.equals(Format.CONSTANT)){
                        pieces.addAll(parseFormat(matcher.group(1), index + 1));
                    }

                    pieces.add(format.generator(format.arguments(matcher)));

                    // constant SHOULD catch the entire row and leave nothing else
                    if(!format.equals(Format.CONSTANT)){
                        pieces.addAll(parseFormat(matcher.group(matcher.groupCount()), index));
                    }
                } else {
                    pieces.addAll(parseFormat(row, index + 1));
                }
            }
        }

        return pieces;
    }

    /**
     * Get Format Specifier Message.
     * @return Specifier Message
     */
    private static String getFormatSpecifiersMessage() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Format Specifiers:\n");
        for(final Format format : Format.values()){
            if(!"CONSTANT".equals(format.toString())) {
                sb.append('\t').append(format).append('\n');
            }
        }
        return sb.toString();
    }
}
