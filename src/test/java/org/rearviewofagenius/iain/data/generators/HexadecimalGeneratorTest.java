package org.rearviewofagenius.iain.data.generators;

import org.junit.Assert;
import org.junit.Test;
import org.rearviewofagenius.iain.data.Format;

public class HexadecimalGeneratorTest extends GeneratorTest {
    @Test
    public void BinaryHex(){
        BinaryGenerator.setBinaryFormat("hex");
        testGenerator(Format.BINARY, "BINARY", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not properly prefixed: " + value, "0x", value.substring(0, 2));
            Assert.assertEquals(format.toString() + " generated a value that was not a hexadecimal value: " + value.substring(2, value.length()).replaceAll("[0-9A-F]", ""), "", value.substring(2, value.length()).replaceAll("[0-9A-F]", ""));
        });
    }
}
