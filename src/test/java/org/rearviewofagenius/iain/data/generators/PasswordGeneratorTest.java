package org.rearviewofagenius.iain.data.generators;

import org.junit.Assert;
import org.junit.Test;
import org.rearviewofagenius.iain.data.Format;

public class PasswordGeneratorTest extends GeneratorTest {

    @Test
    public void Password() {
        System.out.println("Testing PasswordGenerator");
        testGenerator(Format.PASSWORD, "PASSWORD", (value, format) -> {
            Assert.assertTrue(format.toString() + " generated a non-string", value instanceof String);
        });
    }

    @Test
    public void PasswordCount() {
        for(int wordCount : new int[] {1,2,3,4}) {
            testGenerator(Format.PASSWORD_COUNT, "PASSWORD(" + wordCount + ")", (value, format) -> {
                Assert.assertTrue(format.toString() + " generated a non-string", value instanceof String);
            });
        }
    }

    @Test
    public void PasswordCountLength() {
        for(int wordCount : new int[] {1,2,4}) {
            for(int wordLength : new int[] {4,8,16}) {
                testGenerator(Format.PASSWORD_COUNT_LENGTH, "PASSWORD(" + wordCount + ", " + wordLength + ")", (value, format) -> {
                    int maximumPasswordLength = wordLength * wordCount + wordCount;
                    if(wordCount == 1 && wordLength < PasswordGenerator.MINIMUM_PASSWORD_LENGTH){
                        maximumPasswordLength = (int)Math.round(PasswordGenerator.MINIMUM_PASSWORD_LENGTH + 1);
                    }
                    Assert.assertTrue(format.toString() + " generated a non-string", value instanceof String);
                    Assert.assertTrue(format.toString() + " generated a string of the wrong length, \"" + value + "\"(" + value.length() + "), not greater than " + PasswordGenerator.MINIMUM_PASSWORD_LENGTH, value.length() >= PasswordGenerator.MINIMUM_PASSWORD_LENGTH);
                });
            }
        }
    }


}
