package org.rearviewofagenius.iain.data.generators;

import org.junit.Assert;
import org.junit.Test;
import org.rearviewofagenius.iain.data.Format;

public class OctetGeneratorTest extends GeneratorTest {
    @Test
    public void BinaryOctet(){
        BinaryGenerator.setBinaryFormat("octet");
        testGenerator(Format.BINARY, "BINARY", (value, format) -> {
            for(String v:value.split("\\\\")){
                if(!v.isEmpty()){
                    Integer digit = Integer.valueOf(v.substring(0, 1));
                    Assert.assertTrue(format.toString() + " generated a value that was outside the range of octet values: " + v, digit >= 0);
                    Assert.assertTrue(format.toString() + " generated a value that was outside the range of octet values: " + v, digit < 4);

                    digit = Integer.valueOf(v.substring(1, 2));
                    Assert.assertTrue(format.toString() + " generated a value that was outside the range of octet values: " + v, digit >= 0);
                    Assert.assertTrue(format.toString() + " generated a value that was outside the range of octet values: " + v, digit < 8);

                    digit = Integer.valueOf(v.substring(2, 3));
                    Assert.assertTrue(format.toString() + " generated a value that was outside the range of octet values: " + v, digit >= 0);
                    Assert.assertTrue(format.toString() + " generated a value that was outside the range of octet values: " + v, digit < 8);
                }
            }
        });
    }
}
