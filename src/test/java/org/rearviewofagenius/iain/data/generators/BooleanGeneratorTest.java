package org.rearviewofagenius.iain.data.generators;

import org.junit.Assert;
import org.junit.Test;
import org.rearviewofagenius.iain.data.Format;

public class BooleanGeneratorTest extends GeneratorTest {
    @Test
    public void Boolean(){
        testGenerator(Format.BOOLEAN,"BOOLEAN",
            (value,format) -> { Assert.assertTrue(format.toString() + " generated a value that was not true or false", value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")); });
    }
}
