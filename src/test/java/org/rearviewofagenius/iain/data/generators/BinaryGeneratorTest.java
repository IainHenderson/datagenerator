package org.rearviewofagenius.iain.data.generators;

import org.junit.Assert;
import org.junit.Test;
import org.rearviewofagenius.iain.data.Format;

public class BinaryGeneratorTest extends GeneratorTest {
    @Test
    public void Binary(){
        BinaryGenerator.setBinaryFormat("bitstring");
        testGenerator(Format.BINARY, "BINARY", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]",""), "", value.replaceAll("[0-1]",""));
        });
    }

    @Test
    public void BinaryBits(){
        BinaryGenerator.setBinaryFormat("bitstring");
        testGenerator(Format.BINARY_BITS, "BINARY(1 Bits)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]", ""), "", value.replaceAll("[0-1]", ""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 1);
        });
        testGenerator(Format.BINARY_BITS, "BINARY(2 Bits)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]", ""), "", value.replaceAll("[0-1]", ""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 2);
        });
        testGenerator(Format.BINARY_BITS, "BINARY(3 Bits)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]", ""), "", value.replaceAll("[0-1]", ""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 3);
        });
        testGenerator(Format.BINARY_BITS, "BINARY(4 Bits)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]", ""), "", value.replaceAll("[0-1]", ""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 4);
        });
        testGenerator(Format.BINARY_BITS, "BINARY(5 Bits)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]", ""), "", value.replaceAll("[0-1]", ""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 5);
        });
        testGenerator(Format.BINARY_BITS, "BINARY(6 Bits)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]", ""), "", value.replaceAll("[0-1]", ""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 6);
        });
        testGenerator(Format.BINARY_BITS, "BINARY(7 Bits)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]", ""), "", value.replaceAll("[0-1]", ""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 7);
        });
        testGenerator(Format.BINARY_BITS, "BINARY(8 Bits)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]", ""), "", value.replaceAll("[0-1]", ""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 8);
        });
    }

    @Test
    public void BinaryLength(){
        BinaryGenerator.setBinaryFormat("bitstring");
        testGenerator(Format.BINARY_LENGTH,"BINARY(1)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]",""), "", value.replaceAll("[0-1]",""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 8);
        });
        testGenerator(Format.BINARY_LENGTH,"BINARY(2)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]",""), "", value.replaceAll("[0-1]",""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 16);
        });
        testGenerator(Format.BINARY_LENGTH,"BINARY(3)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]",""), "", value.replaceAll("[0-1]",""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 24);
        });
        testGenerator(Format.BINARY_LENGTH,"BINARY(4)", (value, format) -> {
            Assert.assertEquals(format.toString() + " generated a value that was not binary: " + value.replaceAll("[0-1]",""), "", value.replaceAll("[0-1]",""));
            Assert.assertTrue("Length was " + value.length(), value.length() == 32);
        });
    }

}
