package org.rearviewofagenius.iain.data.generators;

import org.junit.Assert;
import org.junit.Test;
import org.rearviewofagenius.iain.data.Format;

public class ConstantGeneratorTest extends GeneratorTest {
    @Test
    public void Constant(){
        testGenerator(Format.CONSTANT, "Constant 1", (value, format) -> {
                Assert.assertEquals(format.toString() + " generated an unexpected value", value, "Constant 1");
            }
        );
        testGenerator(Format.CONSTANT, "Constant 2", (value, format) -> {
                Assert.assertEquals(format.toString() + " generated an unexpected value", value, "Constant 2");
            }
        );
        testGenerator(Format.CONSTANT, "Constant 3", (value, format) -> {
                Assert.assertEquals(format.toString() + " generated an unexpected value", value, "Constant 3");
            }
        );
    }
}
