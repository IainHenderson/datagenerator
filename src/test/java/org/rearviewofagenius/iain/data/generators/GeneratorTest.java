package org.rearviewofagenius.iain.data.generators;

import java.util.function.BiConsumer;
import org.junit.Assert;
import org.rearviewofagenius.iain.data.Format;

public abstract class GeneratorTest {
	private static final Integer ROWS = 10000;

    void testGenerator(final Format format, final String row, final BiConsumer<String, Format> valueEvaluator){
		Assert.assertTrue(format.toString() + " does match " + row, format.matches(row));
//        Generator.setVerbose(true);
		Generator generator = format.generator(row);
		String value = null;
		for(int i = 0; i <= ROWS; i++){
			try{
				value = generator.next(); // give the generator a chance to start
			} catch(InterruptedException ie) { 
				System.err.println(ie.getMessage());
			}
            Assert.assertNotNull(format.toString() + " created " + i + " values for " + row, value);
            valueEvaluator.accept(value, format);
		}
	}
}
